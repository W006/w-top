package com.wennn.top.generator.config;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

public class GeneratorConfig {

    public AutoGenerator autoGenerator() {
        AutoGenerator generator = new AutoGenerator();

        // set freemarker engine
        generator.setTemplateEngine(new FreemarkerTemplateEngine());
        return generator;
    }
}
