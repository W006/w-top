package com.wennn.top.generator.entity;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import lombok.Data;

@Data
public class SmGenerator {

    private static final String standPackagePath="/src/main/java";

    /**数据库配置*/
    private String dbUrl;
    private String dbDriverName;
    private String dbUserName;
    private String dbPassword;



    /**工程路径*/
    private String projectPath;

    /**全局配置*/
    private GlobalConfig globalConfig;

    /**包配置*/
    private PackageConfig packageConfig;

    private DataSourceConfig dataSourceConfig;



}
