package com.wennn.top.generator;

import com.wennn.top.generator.util.GeneratorBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class WGeneratorApplicationTests {

    @Test
    void contextLoads() {
        GeneratorBuilder.builder("root","root",
                "jdbc:mysql://127.0.0.1/yxshop?useUnicode=true&characterEncoding=UTF-8","com.mysql.cj.jdbc.Driver")
                .setGlobalConfig(null,"wennn")
                .setPackageConfig("sm","com.wennn.top.temp.biz")
                .build("user");
    }

}
