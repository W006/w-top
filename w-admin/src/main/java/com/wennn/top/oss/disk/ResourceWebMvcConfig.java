package com.wennn.top.oss.disk;

import com.wennn.top.oss.CloudStorageConfig;
import com.wennn.top.oss.Constant;
import com.wennn.top.oss.config.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description:
 * @author: wennn
 * @date: 10/10/20
 */
@Component
public class ResourceWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    ConfigService configService;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        CloudStorageConfig configObject = configService.getConfigObject(Constant.CloudService.DISCK.toString());

        registry.addResourceHandler("/files").addResourceLocations(configObject.getDiskPath());
    }
}
