package com.wennn.top.oss.exception;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.exception.WBusiException;

/**
 * @description:
 * @author: wennn
 * @date: 9/24/20
 */
public class OssException extends WBusiException {

    Throwable throwable;

    public OssException() {
        super();
    }

    public OssException(String msg) {
        super(msg);
    }

    public OssException(String msg,Throwable e) {
        super(msg);
        this.throwable = e;
    }


    public OssException(IResult result) {
        super(result);
    }

}
