package com.wennn.top.oss.config;

import com.wennn.top.oss.CloudStorageConfig;
import com.wennn.top.oss.Constant;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: wennn
 * @date: 10/1/20
 */

@Service
public class ConfigServiceImpl implements ConfigService {

    @Override
    public CloudStorageConfig getConfigObject(String key) {

        CloudStorageConfig config = new CloudStorageConfig();

        config.setType(Constant.CloudService.DISCK.getValue());
        config.setDiskPath("/Users/wen/Downloads/upload");
        config.setProxyServer("http://localhost:8866/files");

        return config;
    }
}
