package com.wennn.top.oss;

import com.wennn.top.oss.aliyun.AliyunCloudStorageService;
import com.wennn.top.oss.config.ConfigService;
import com.wennn.top.oss.config.ConfigServiceImpl;
import com.wennn.top.oss.disk.DiskCloudStorageService;
import com.wennn.top.oss.qcloud.QcloudCloudStorageService;
import com.wennn.top.oss.qiniu.QiniuCloudStorageService;

/**
 * 文件上传Factory
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-26 10:18
 */
public final class OSSFactory {
    private static ConfigService sysConfigService;

    static {
        OSSFactory.sysConfigService = new ConfigServiceImpl(); // (ConfigService) SpringUtils.getBean("configServiceImpl");
    }

    public static CloudStorageService build() {
        //获取云存储配置信息
        CloudStorageConfig config = sysConfigService.getConfigObject(Constant.CLOUD_STORAGE_CONFIG_KEY);

        if (config.getType() == Constant.CloudService.QINIU.getValue()) {
            return new QiniuCloudStorageService(config);
        } else if (config.getType() == Constant.CloudService.ALIYUN.getValue()) {
            return new AliyunCloudStorageService(config);
        } else if (config.getType() == Constant.CloudService.QCLOUD.getValue()) {
            return new QcloudCloudStorageService(config);
        } else if (config.getType() == Constant.CloudService.DISCK.getValue()) {
            return new DiskCloudStorageService(config);
        }

        return null;
    }

}
