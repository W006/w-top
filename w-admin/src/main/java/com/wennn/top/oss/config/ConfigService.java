package com.wennn.top.oss.config;

import com.wennn.top.oss.CloudStorageConfig;

public interface ConfigService {

    public CloudStorageConfig getConfigObject(String key);

}
