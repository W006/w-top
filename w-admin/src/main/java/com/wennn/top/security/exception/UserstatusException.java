package com.wennn.top.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @description:
 * @author: wennn
 * @date: 6/14/20
 */
public class UserstatusException extends AuthenticationException {
    public UserstatusException(String msg, Throwable t) {
        super(msg, t);
    }

    public UserstatusException(String msg) {
        super(msg);
    }
}
