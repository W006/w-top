package com.wennn.top.security.handler;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.security.service.SessionUserService;
import com.wennn.top.security.util.JwtUtil;
import com.wennn.top.security.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class OutSuccessHandler implements LogoutSuccessHandler, LogoutHandler {

    SessionUserService sessionUserService;

    public OutSuccessHandler(SessionUserService sessionUserService){
        this.sessionUserService = sessionUserService;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        log.debug("退出成功。。。。。。。。。。。");
        WebUtil.response(httpServletResponse,IResult.ok().msg("退出成功"));
    }

    /**
     * 退出登陆
     * @param httpServletRequest
     * @param httpServletResponse
     * @param authentication
     */
    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        log.debug("开始退出登陆 ");
        String token = JwtUtil.getRequestToken(httpServletRequest);
        sessionUserService.removeSession(token);
        log.debug("结束退出登陆 ");
    }
}
