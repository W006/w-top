package com.wennn.top.security.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @description: jwt 配置信息
 * @author: wennn
 * @date: 5/9/20 4:08 PM
 */
@Data
@ConfigurationProperties(prefix = "jwt")
@Configuration
public class JwtSetting {

    /**
     * 过期时间 分钟
     */
    private Integer tokenExpirationTime;

    /**
     * 发布时间
     */
    private String tokenIssuer;

    /**
     * 签名key
     */
    private String tokenSigningKey;

    /**
     * 刷新时间
     */
    private Integer refreshTokenExpTime;

    /**
     * redis 缓存时间  这个是用于用户最久session有效期 分钟
     */
    private Integer tokenRedisExpirationTime;

    /**
     * 用户放到Redis 中的key前缀
     */
    private String onlineKey;
}
