package com.wennn.top.security.model;

import com.google.common.collect.Maps;
import com.wennn.top.security.vo.AuthUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;


/**
 * @description: session用户信息
 * @author: wennn
 * @date: 5/9/20 3:19 PM
 */
@Data
public class SessionUser implements Serializable {

    private Integer id;

    private String username;

    private String account;

    private String nickname;

    private String phone;

    private Integer birthday;

    private String token;

    private String avatar;

    private Long loginTime;

    private String key;

    private Map props = Maps.newHashMap();

    /**
     * 用户扩展筛入其他属性值的
     *
     * @param key   属性名称
     * @param value 值
     * @return 放回当前用户
     */
    public SessionUser set(String key, Object value) {
        this.props.put(key, value);
        return this;
    }

    public static SessionUser builder(AuthUser userDetails) {
        if (Optional.ofNullable(userDetails).isPresent()) {
            SessionUser sessionUser = userDetails.getSessionUser();
            return sessionUser;
        }
        return null;
    }

    public static SessionUser builder() {
        return new SessionUser();
    }

}
