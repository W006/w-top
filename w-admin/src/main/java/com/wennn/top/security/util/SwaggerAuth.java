package com.wennn.top.security.util;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Sets;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import springfox.documentation.builders.OperationBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiDescription;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ApiListingScannerPlugin;
import springfox.documentation.spi.service.contexts.DocumentationContext;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * 自定义 swagger 说明
 *
 * @author: wennn
 * @date: 7/13/20 8:37 PM
 */
public class SwaggerAuth implements ApiListingScannerPlugin {
    @Override
    public List<ApiDescription> apply(DocumentationContext documentationContext) {
        HashSet<String> tags = Sets.newHashSet("建权管理");
        return new ArrayList<ApiDescription>(
                Arrays.asList(
                        new ApiDescription(
                                SecurityConst.AUTH_LOGIN_URL,  //url
                                "用户登陆", //描述
                                Arrays.asList(
                                        new OperationBuilder(
                                                new CachingOperationNameGenerator())
                                                .method(HttpMethod.POST)//http请求类型
                                                .produces(Sets.newHashSet(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                                                .summary("获取token")
                                                .notes("获取token")//方法描述
                                                .tags(tags)//归类标签
                                                .parameters(
                                                        Arrays.asList(
                                                                new ParameterBuilder()
                                                                        .description("用户名")
                                                                        .type(new TypeResolver().resolve(String.class))
                                                                        .name("username")
                                                                        .parameterType("query")
                                                                        .parameterAccess("access")
                                                                        .required(true)
                                                                        .modelRef(new ModelRef("string")) //<5>
                                                                        .build(),
                                                                new ParameterBuilder()
                                                                        .description("密码")
                                                                        .type(new TypeResolver().resolve(String.class))
                                                                        .name("password")
                                                                        .parameterType("query")
                                                                        .parameterAccess("access")
                                                                        .required(true)
                                                                        .modelRef(new ModelRef("string")) //<5>
                                                                        .build()
                                                        ))
                                                .build()

                                ), false),
                        new ApiDescription(
                                SecurityConst.AUTH_LOGOUT_URL,  //url
                                "用户退出登陆", //描述
                                Arrays.asList(
                                        new OperationBuilder(new CachingOperationNameGenerator())
                                                .method(HttpMethod.GET)
                                                .summary("退出登陆")
                                                .tags(tags)
                                                .notes("退出登陆,清空登陆信息")
                                                .build()
                                ), false)
                ));
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return DocumentationType.SWAGGER_2.equals(documentationType);
    }
}
