package com.wennn.top.security.util;

public class SecurityConst {

    /**
     * 签名key
     */
    public static final String SIGNING_KEY = "spring-security-@Jwt!&Secret^#";

    /**
     * 头信息key
     */
    public static final String AUTH_HEADER = "Authorization";

    /**
     * 头信息内容 key
     */
    public static final String AUTH_HEADER_PRE = "Bearer ";

    /**
     * 用户角色信息存放地方
     */
    public static final String JWT_CLAIMS_SCOPE_KEY = "jc_scopes";

    /**
     * 用户信息存放的key
     */
    public static final String JWT_CLAIMS_USER_KEY = "jc_user";

    /**
     * 登陆url
     */
    public static final String AUTH_LOGIN_URL="/login";
    /**
     * 退出登陆url
     */
    public static final String AUTH_LOGOUT_URL="/logout";
}
