package com.wennn.top.security.util;

import com.wennn.top.util.JsonUtil;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @description:
 * @author: wennn
 * @date: 6/13/20
 */
public class WebUtil {


    public static void response(HttpServletResponse httpServletResponse, Object body) throws IOException {
        httpServletResponse.setContentType("text/xml;charset=UTF-8");
        httpServletResponse.setHeader("Cache-Control", "no-cache");
        IOUtils.write(JsonUtil.toJson(body),httpServletResponse.getOutputStream(), StandardCharsets.UTF_8.name());
    }
}
