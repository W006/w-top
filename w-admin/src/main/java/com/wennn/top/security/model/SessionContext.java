package com.wennn.top.security.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

/**
 * @description: Session信息
 * @author: wennn
 * @date: 5/9/20 3:23 PM
 */

public class SessionContext {

    /**
     * 用户信息
     */
    @Getter
    @Setter
    SessionUser sessionUser;

    /**
     * 权限信息
     */
    @Getter
    Collection<? extends GrantedAuthority> authorities;

    public String getUsername() {
        return this.sessionUser == null ? null : this.sessionUser.getUsername();
    }

    public static SessionContext builder(SessionUser sessionUser, Collection<? extends GrantedAuthority> authorities) {
        SessionContext sessionContext = new SessionContext();
        sessionContext.setSessionUser(sessionUser);
        sessionContext.setAuthorities(authorities);
        return sessionContext;
    }

    public static SessionContext builder(SessionUser sessionUser) {
        SessionContext sessionContext = new SessionContext();
        sessionContext.setSessionUser(sessionUser);
        sessionContext.setAuthorities(Collections.EMPTY_LIST);
        return sessionContext;
    }

    public SessionContext setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
        return this;
    }

}
