package com.wennn.top.security.point;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.security.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用来解决匿名用户访问无权限资源时的异常
 * @Auther: zhaoxinguo
 * @Date: 2018/9/20 14:55
 * @Description: 自定义认证拦截器
 */
@Slf4j
public class Http401AuthEntryPoint implements AuthenticationEntryPoint {

    private final String headerValue;

    public Http401AuthEntryPoint(String headerValue) {
        this.headerValue = headerValue;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        WebUtil.response(response,IResult.fail(HttpServletResponse.SC_UNAUTHORIZED+"",authException.getMessage()));
    }

}

