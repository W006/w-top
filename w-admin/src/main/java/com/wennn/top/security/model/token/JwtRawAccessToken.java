package com.wennn.top.security.model.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @description: 访问令牌
 * @author: wennn
 * @date: 5/9/20
 */

@Data
@AllArgsConstructor
public class JwtRawAccessToken implements JwtToken {

    private String token;

    /**
     * @description:  解析token
     * @param: 签名
     * @author: wennn
     * @date: 5/9/20 5:19 PM
     */
    public Jws<Claims> parseClaims(String signingKey){
       return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(this.token);
    }

}
