package com.wennn.top.security.vo;


import com.wennn.top.security.model.SessionUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Collections;

@Data
public class AuthUser extends User {

    private String username;
    private String password;

    private SessionUser sessionUser;

    public AuthUser(SessionUser sessionUser,String password){
        this(sessionUser,password, Collections.emptyList());
    }

    public AuthUser(SessionUser sessionUser,String password, Collection<? extends GrantedAuthority> authorities) {
        super(sessionUser.getUsername(), password, true, true, true, true, authorities);
        this.sessionUser = sessionUser;
        this.username = this.sessionUser.getUsername();
        this.password = password;
    }
}
