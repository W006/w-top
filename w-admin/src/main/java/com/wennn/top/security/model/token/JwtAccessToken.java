package com.wennn.top.security.model.token;

import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtAccessToken implements JwtToken{

    private String token;

    private Claims claims;

}
