package com.wennn.top.security.model.token;

public interface JwtToken {

    String getToken();
}
