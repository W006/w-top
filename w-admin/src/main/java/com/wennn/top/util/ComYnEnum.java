package com.wennn.top.util;

/**
 * 针对bit类型
 * @author: wennn
 * @date: 5/15/20 9:10 PM
 */
public enum ComYnEnum {
    YES(true),NO(false);
    Boolean flag;
    ComYnEnum(Boolean flag){
        this.flag = flag;
    }
}
