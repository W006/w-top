package com.wennn.top.util;

import com.wennn.top.generator.util.GeneratorBuilder;
import com.wennn.top.generator.util.PropUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Properties;

@Slf4j
public class GeneratorUtil {

    public static void main(String[] args) {

        Properties prop = getDefaultProps();

        String username = prop.get("spring.datasource.username") + "";
        String password = prop.get("spring.datasource.password") + "";
        String url = prop.get("spring.datasource.url") + "";
        String driverName = prop.get("spring.datasource.driver-class-name") + "";

        log.debug("用户名:{},密码:{},URL:{},DriverName:{}", username, password, url, driverName);

        GeneratorBuilder builder = GeneratorBuilder
                .builder(username, password, url, driverName)
                //.builder("root","root","jdbc:mysql://127.0.0.1/yxshop?useUnicode=true&characterEncoding=UTF-8","com.mysql.cj.jdbc.Driver")
                .setGlobalConfig(null, "wennn")
                .enableSwagger();
        builder.getTemplateConfig().setController("");
        builder.getTemplateConfig().setXml("");
//        menuTables(builder);
//        orderTables(builder);
//        productTables(builder);
//        userTables(builder);
//        couponTables(builder);
        systemTables(builder);
    }


    public static Properties getDefaultProps() {
        Properties properties = PropUtils.getEnvYaml("application-dev");
        if (null == properties) {
            properties = PropUtils.getEnvProp("application-dev");
        }
        return properties;
    }

    //   产品表
    public static void productTables(GeneratorBuilder builder) {
        builder.setPackageConfig("product", "com.wennn.top.shop.biz").build(
                "YX_STORE_CATEGORY"
//                "yx_store_product,yx_store_product_attr,yx_store_product_attr_result," +
//                        "yx_store_product_attr_value,yx_store_product_cate," +
//                        "yx_store_product_relation,yx_store_product_reply"
        );
    }

    //    用户表
    public static void userTables(GeneratorBuilder builder) {
        builder.setPackageConfig("user", "com.wennn.top.shop.biz").build(
                "yx_user,yx_user_bill,yx_user_sign,yx_user_enter,yx_user_group,yx_user_level,yx_user_address,yx_user_extract,yx_user_recharge,yx_user_task_finish"
        );
    }

    //    订单表
    public static void orderTables(GeneratorBuilder builder) {

        builder.setPackageConfig("order", "com.wennn.top.shop.biz").build(
                "YX_STORE_CART"
        );
    }

    //   菜单
    public static void menuTables(GeneratorBuilder builder) {

        builder.setPackageConfig("menu", "com.wennn.top.shop.biz").build(
                "menu"
        );
    }

    //   优惠券
    public static void couponTables(GeneratorBuilder builder) {

        builder.setPackageConfig("coupon", "com.wennn.top.shop.biz").build(
                "YX_STORE_COUPON,YX_STORE_COUPON_USER,YX_STORE_COUPON_ISSUE_USER,YX_STORE_COUPON_ISSUE"
        );
    }

    //  系统模块
    public static void systemTables(GeneratorBuilder builder) {

        builder.setPackageConfig("system", "com.wennn.top.shop.biz").build(
                "YX_SYSTEM_CONFIG,YX_SYSTEM_STORE,YX_SYSTEM_ATTACHMENT,YX_SYSTEM_USER_TASK,YX_SYSTEM_GROUP_DATA,YX_SYSTEM_USER_LEVEL"
        );
    }


}
