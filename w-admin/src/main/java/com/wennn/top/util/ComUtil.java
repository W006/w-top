package com.wennn.top.util;

import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;

/**
 * @description:
 * @author: wennn
 * @date: 5/26/20
 */
@Slf4j
public class ComUtil {

    private static final String all_item = "abcdefghijklmnopqrstuvwxwzABCDEFGHIJKLMNOPQRSTUVWXWZ,`!@#$%^&*1234567890";

    /**
     * 随机生成密码
     *
     * @author: wennn
     * @date: 5/26/20 5:36 PM
     */
    public static String randomPwd() {
        Random random = new Random();
        int max = random.nextInt(2) + 8;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < max; i++) {
            stringBuilder.append(all_item.charAt(random.nextInt(all_item.length())));
        }
        return stringBuilder.toString();
    }


    /**
     * base64加密
     *
     * @author: wennn
     * @date: 5/26/20 5:36 PM
     */
    public static String encode64(String enstr) {
        byte[] encode = Base64.getEncoder().encode(enstr.getBytes(StandardCharsets.UTF_8));
        return new String(encode);
    }

    /**
     * base64解密
     *
     * @author: wennn
     * @date: 5/26/20 5:36 PM
     */
    public static String decode64(String enstr) {
        byte[] destr = Base64.getDecoder().decode(enstr.getBytes(StandardCharsets.UTF_8));
        return new String(destr);
    }



}
