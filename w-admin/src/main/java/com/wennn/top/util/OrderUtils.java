package com.wennn.top.util;

import com.google.common.collect.Maps;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * @description: 订单工具类
 * @author: wennn
 * @date: 7/9/20
 */

public class OrderUtils {
    final static Map<String,String> s = Maps.newHashMap();

    final static CountDownLatch latch = new CountDownLatch(2);

    public static void main(String[] args) {
        System.out.println(getOrderCode());
        System.out.println(getRandomKey());
    }

    /**
     * 生成订单号
     * @return String
     */
    public static String getOrderCode() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int a = (int)(Math.random() * 9000.0D) + 1000;
        Date date = new Date();
        String str = sdf.format(date);
        String[] split = str.split("-");
        String s = split[0] + split[1] + split[2];
        String[] split1 = s.split(" ");
        String s1 = split1[0] + split1[1];
        String[] split2 = s1.split(":");
        return split2[0] + split2[1] + split2[2] + a;
    }

    /**
     * 随机生成字符串
     * @return
     */
    public static String getRandomKey(){
       return UUID.randomUUID().toString().replace("-","");
    }
}
