package com.wennn.top.sms.mail.service;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.sms.mail.dto.MailDto;

public interface JMailService {

    /**
     * 发送邮件
     * @param receivers 接收者
     * @param context   发送内容
     * @return
     */
    public IResult sendMail(String receivers,String context);

    public IResult sendMail(String deliver,String receivers,String content);

    public IResult sendMail(String deliver,String receivers, String cc, String content);

    public IResult sendMail(MailDto mailDto);

}
