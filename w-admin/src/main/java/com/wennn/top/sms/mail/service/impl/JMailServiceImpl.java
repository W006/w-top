package com.wennn.top.sms.mail.service.impl;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.sms.mail.dto.MailDto;
import com.wennn.top.sms.mail.service.JMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: wennn
 * @date: 5/26/20
 */
@Component
@Slf4j
public class JMailServiceImpl implements JMailService {

    @Override
    public IResult sendMail(String receivers, String context) {
        log.debug("发送邮件》接收{}，内容：{}",receivers,context);
        return IResult.ok();
    }

    @Override
    public IResult sendMail(String deliver, String receivers, String content) {
        return null;
    }

    @Override
    public IResult sendMail(String deliver, String receivers, String cc, String content) {
        return null;
    }

    @Override
    public IResult sendMail(MailDto mailDto) {
        return null;
    }
}
