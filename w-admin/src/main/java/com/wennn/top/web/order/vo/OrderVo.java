package com.wennn.top.web.order.vo;

import com.wennn.top.shop.biz.order.entity.YxStoreOrder;
import com.wennn.top.web.product.vo.CartInfoVo;
import lombok.Data;

/**
 * @description: 订单详情使用
 * @author: wennn
 * @date: 7/18/20
 */
@Data
public class OrderVo extends YxStoreOrder {

    private CartInfoVo cartInfoVo;
}
