package com.wennn.top.web.product;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.mybatis.vo.PageResult;
import com.wennn.top.core.util.ErrorHelper;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.order.service.IYxStoreCartService;
import com.wennn.top.shop.biz.product.entity.YxStoreCategory;
import com.wennn.top.shop.biz.product.entity.YxStoreProduct;
import com.wennn.top.shop.biz.product.entity.YxStoreProductRelation;
import com.wennn.top.shop.biz.product.es.RelationType;
import com.wennn.top.shop.biz.product.service.IYxStoreCategoryService;
import com.wennn.top.shop.biz.product.service.IYxStoreProductRelationService;
import com.wennn.top.shop.biz.product.service.IYxStoreProductService;
import com.wennn.top.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/product")
@Api(tags = "商品")
public class StoreProductRest {

    @Autowired
    IYxStoreProductService iYxStoreProductService;

    @Autowired
    IYxStoreCategoryService iYxStoreCategoryService;

    @Autowired
    IYxStoreCartService iYxStoreCartService;

    @Autowired
    IYxStoreProductRelationService iYxStoreProductRelationService;

    @ApiOperation("获取商品分类信息")
    @PostMapping(value = "/classify")
    public IResult classify(YxStoreCategory yxStoreCategory) {
        List tree = iYxStoreCategoryService.getTree(yxStoreCategory);
        return IResult.ok(tree);
    }

    @ApiOperation("获取分类下的产品")
    @PostMapping(value = "/classifyProduct/{cateId}")
    public IResult classifyProduct(@PathVariable(value="cateId") String pId,
                                   @RequestParam(value="pageNo",defaultValue = "1") long pageNo,
                                   @RequestParam(value="pageSize",defaultValue = "100") long pageSize,
                                   @RequestParam(value="keyWord",required = false) String key
                                   ) {

        Page<YxStoreProduct> page = new Page(pageNo,pageSize);
        LambdaQueryWrapper<YxStoreProduct> storeWrapper = Wrappers.lambdaQuery();

        storeWrapper.eq(YxStoreProduct::getCateId,pId).orderByAsc(YxStoreProduct::getIsHot,YxStoreProduct::getSort);
        Page<YxStoreProduct> page1 = iYxStoreProductService.page(page, storeWrapper);
        return PageResult.builder(page1);
    }


    @ApiOperation("获取商品详情")
    @PostMapping(value = "/detail/{id}")
    public IResult detail(@PathVariable("id") String id) {
        Map map = Maps.newHashMap();
        LambdaQueryWrapper<YxStoreProduct> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(YxStoreProduct::getId, id);
        YxStoreProduct one = iYxStoreProductService.getOne(queryWrapper);

        //商品信息 商品详情
        //商品属性
        //商品属性值
        //店铺信息
        //评论


        return ErrorHelper.ifPresent(one);
    }


    @ApiOperation("当前登陆人 收藏商品")
    @PostMapping(value = "/collect/add/{id}")
    public IResult collect(@RequestParam(value="category") String category,
                           @PathVariable(value="id") int id
    ) {
        YxStoreProductRelation relation = new YxStoreProductRelation();
        relation.setUid(ContextUtil.getUid());
        relation.setProductId(id);
        relation.setCategory(category);
        relation.setType(RelationType.COLLECT.getCode());
        QueryWrapper<YxStoreProductRelation> query = Wrappers.query(relation);

        if(iYxStoreProductRelationService.count(query) > 0){
            return IResult.ok().msg("已经添加");
        }

        relation.setAddTime(DateUtil.nowIntTime());
        iYxStoreProductRelationService.save(relation);
        return IResult.ok();
    }

    @ApiOperation("当前登陆人 获取收藏的商品")
    @PostMapping(value = "/get/collect")
    public IResult getCollect() {
        LambdaQueryWrapper<YxStoreProductRelation> queryWrapper = Wrappers.lambdaQuery(YxStoreProductRelation.class).eq(YxStoreProductRelation::getUid,ContextUtil.getUid());
        List<YxStoreProductRelation> list = iYxStoreProductRelationService.list(queryWrapper);
        return IResult.ok(list);
    }


    @ApiOperation("当前登陆人 删除收藏的商品")
    @PostMapping(value = "/collect/del/{id}")
    public IResult collectDel(@RequestParam(value="category") String category,
                           @PathVariable(value="id") int id
    ) {
        LambdaQueryWrapper<YxStoreProductRelation> relWrapper = Wrappers.lambdaQuery(YxStoreProductRelation.class);
        relWrapper.eq(YxStoreProductRelation::getUid,ContextUtil.getUid()).eq(YxStoreProductRelation::getCategory,category).eq(YxStoreProductRelation::getProductId,id);
        boolean remove = iYxStoreProductRelationService.remove(relWrapper);
        return IResult.ok(remove);
    }








}
