package com.wennn.top.web.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 支付价格=总价+邮费-优惠券-扣除
 * @author: wennn
 * @date: 7/8/20
 */
@Data
public class OrderComputedVo {
    /**优惠券价格*/
    private BigDecimal couponPrice;
    /**扣除价格*/
    private BigDecimal deductionPrice;
    /**邮费*/
    private BigDecimal payPostage;
    /**支付价格*/
    private BigDecimal payPrice;
    /**总价格*/
    private BigDecimal totalPrice;
    /**积分扣减*/
    private BigDecimal usedIntegral;
 
}
