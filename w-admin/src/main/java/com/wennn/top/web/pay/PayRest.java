package com.wennn.top.web.pay;

import com.wennn.top.common.vo.IResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: wennn
 * @date: 7/9/20
 */

@RestController
@RequestMapping("/pay")
@Api(tags = "支付模块",description = "用户支付信息")
public class PayRest {


    @ApiOperation(value="支付接口",notes = "支付成功-> 通知发货")
    @PostMapping("")
    public IResult pay(){
        return IResult.ok("支付成功");
    }
}
