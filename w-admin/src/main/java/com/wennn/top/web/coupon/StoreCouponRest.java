package com.wennn.top.web.coupon;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wennn.top.common.vo.IResult;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponUser;
import com.wennn.top.shop.biz.coupon.service.IYxStoreCouponUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 优惠券
 * @author: wennn
 * @date: 7/5/20
 */
@RestController
@RequestMapping("/coupon")
public class StoreCouponRest {

    IYxStoreCouponUserService iYxStoreCouponUserService;

    @ApiOperation("获取当前登陆人优惠券")
    @PostMapping(value = "/coupons")
    public IResult coupons(@RequestParam(value="pageNo",defaultValue = "1") long pageNo,
                           @RequestParam(value="pageSize",defaultValue = "100") long pageSize
    ) {
        // coupons

        LambdaQueryWrapper<YxStoreCouponUser> couponUserWrapper = Wrappers.lambdaQuery(YxStoreCouponUser.class);
        couponUserWrapper.eq(YxStoreCouponUser::getUid, ContextUtil.getUid());
        Page page = new Page(pageNo,pageSize);
        page = iYxStoreCouponUserService.page(page, couponUserWrapper);
        return IResult.ok(page);
    }
}
