package com.wennn.top.web.order.dto;

import lombok.Data;

/**
 * @description: 计算订单用的实体
 * @author: wennn
 * @date: 7/8/20
 */

@Data
public class OrderComputedDto {

    /**收获地址*/
    private Integer addressId;
    /**优惠券*/
    private Integer couponId;

    /**购物类型：1快递，2自取*/
    private String shippingType;
    /**是否使用积分*/
    private String useIntegral;
}
