package com.wennn.top.web.order.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 创建订单需要的参数
 * @author: wennn
 * @date: 7/8/20
 */
@Data
public class OrderCreateDto {
    /**收货地址*/
    private Integer addressId;
    /**请求来源*/
    private String from;
    /**优惠券id*/
    private Integer couponId;
    /**备注*/
    private String mark;
    /**支付方式*/
    private String payType;
    /**手机号码*/
    private String phone;
    /**拼团id*/
    private Integer pinkId;
    /**实际名称*/
    private String realName;
    /**购物类型1快递，0自取*/
    private String shippingType;
    /**商户id*/
    private Integer storeId;
    /**积分扣减*/
    private BigDecimal useIntegral;


}
