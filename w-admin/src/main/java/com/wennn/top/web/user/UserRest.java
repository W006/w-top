package com.wennn.top.web.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.collect.Maps;
import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.util.CoreConst;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.order.service.IYxStoreCartService;
import com.wennn.top.shop.biz.order.service.IYxStoreOrderService;
import com.wennn.top.shop.biz.product.service.IYxStoreProductService;
import com.wennn.top.shop.biz.user.entity.YxUser;
import com.wennn.top.shop.biz.user.entity.YxUserAddress;
import com.wennn.top.shop.biz.user.service.IYxUserAddressService;
import com.wennn.top.shop.biz.user.service.IYxUserService;
import com.wennn.top.web.user.dto.YxUserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/user")
@Api(tags = "用户信息")
public class UserRest {

    @Autowired
    IYxUserService iYxUserService;

    @Autowired
    IYxUserAddressService iYxUserAddressService;

    @Autowired
    IYxStoreOrderService iYxStoreOrderService;

    @Autowired
    IYxStoreCartService iYxStoreCartService;

    @Autowired
    IYxStoreProductService iYxStoreProductService;


    @ApiOperation("当前用户地址")
    @PostMapping(value = "/address")
    public IResult address() {
        LambdaQueryWrapper<YxUserAddress> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(YxUserAddress::getUid, ContextUtil.getUser().getId());
        List<YxUserAddress> list = iYxUserAddressService.list(queryWrapper);
        return IResult.ok(list);
    }

    @ApiOperation("获取当前用户信息")
    @PostMapping(value = "/keep/login")
    public IResult checkLogin() {
        return IResult.ok(ContextUtil.getUser());
    }

    @ApiOperation("获取当前用户")
    @PostMapping(value = "/login/user")
    public IResult getLoginUser() {
        Integer id = ContextUtil.getUser().getId();
        YxUser yxUser = iYxUserService.getById(id);
        if (Objects.isNull(yxUser)) {
            return IResult.fail(CoreConst.NO_FOUND, "未找到结果");
        }
        yxUser.setPassword(null);
        yxUser.setPwd(null);
        return IResult.ok(yxUser);
    }


    @ApiOperation("保存用户信息")
    @PostMapping(value = "/save/login/user",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public IResult saveLoginUser(YxUserDto inYxUser,@RequestParam("file") MultipartFile multipartFile) {
       return iYxUserService.modifyLoginUser(inYxUser,multipartFile);
    }


    @ApiOperation("测试")
    @PostMapping(value = "/demo")
    public IResult doDemo() {
        Map result = Maps.newHashMap();
        result.put("user", ContextUtil.getUser());
        result.put("session", ContextUtil.getSession());

        result.put("authors", ContextUtil.getAuthors());
        return IResult.ok(result);
    }

    @ApiOperation("注册用户")
    @PostMapping(value = "/register")
    public IResult register(@RequestBody YxUser yxUser, HttpServletRequest request) {
        yxUser.setAddIp(request.getRemoteHost());
        // 前端加密密码 base64 这里处理
        return iYxUserService.register(yxUser);
    }

    @ApiOperation("忘记密码")
    @PostMapping(value = "/forgetPwd", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public IResult forgetPwd(@RequestParam("username") String username) {
        return iYxUserService.forgetPwd(username);
    }

}
