package com.wennn.top.web.product.vo;

import java.util.HashMap;

/**
 * @description:
 * @author: wennn
 * @date: 7/19/20
 */
public class ProductInfoVo extends HashMap {

//    goodList: []
//    mapKey: "OGABZ-Y5OCF-5UWJ5-N7DHH-VFIG7-DHFEB"
//    merId: 0
//    priceName: ""
//    productAttr: [{id: 31, productId: 29, attrName: "纸张", attrValues: "A4,A3",…},…]
//    productValue: {A3,红色: {id: 57, productId: 29, sku: "A3,红色", stock: 66, sales: 0, price: 7,…},…}
//    reply: {id: null, productId: null, replyType: null, productScore: 5, serviceScore: 5, comment: "haihao",…}
//    replyChance: "100.00"
//    replyCount: 3
//    similarity: []
//    storeInfo: {id: 29, merId: 0, image: "https://image.dayouqiantu.cn/5ca0786c5d2c1.jpg",…}
//    systemStore: {id: 4, name: "信阳门店", introduction: "信阳门店", phone: "15136275234", address: "河南省信阳市",…}
//    tempName: "默认全国运费模板"
//    uid: 0

    /**
     * 设置属性
     * @param value
     * @return
     */
    public ProductInfoVo setAttr(Object value){
        return addParam("productAttr",value);
    }

    /**
     * 设置属性值
     * @param value
     * @return
     */
    public ProductInfoVo setVal(Object value){
        return addParam("productValue",value);
    }

    /**
     * 商品详情
     * @param value
     * @return
     */
    public ProductInfoVo setStoreInfo(Object value){
        return addParam("storeInfo",value);
    }

    /**
     * 商店详情
     * @param value
     * @return
     */
    public ProductInfoVo setSysStore(Object value){
        return addParam("systemStore",value);
    }

    /**
     * 设置评价
     * @param value
     * @return
     */
    public ProductInfoVo setReply(Object value){
        return addParam("reply",value);
    }

    /**
     * 添加其他参数
     * @param key
     * @param value
     * @return
     */
    public ProductInfoVo addParam(String key,Object value){
        return this;
    }
}
