package com.wennn.top.web.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NavMenu {

    private Long id;
    private String name;
    private String pic;
    private String desc;
    private String url;
}
