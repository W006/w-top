package com.wennn.top.web.home;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.mybatis.vo.PageResult;
import com.wennn.top.shop.biz.menu.service.IMenuService;
import com.wennn.top.shop.biz.product.entity.YxStoreCategory;
import com.wennn.top.shop.biz.product.entity.YxStoreProduct;
import com.wennn.top.shop.biz.product.service.IYxStoreCategoryService;
import com.wennn.top.shop.biz.product.service.IYxStoreProductService;
import com.wennn.top.util.ComYnEnum;
import com.wennn.top.web.home.dto.SearchParamDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(tags = "首页功能")
@RestController
@RequestMapping("/home")
public class HomeRest {

    @Autowired
    IYxStoreProductService iYxStoreProductService;

    @Autowired
    IYxStoreCategoryService iYxStoreCategoryService;

    @Autowired
    IMenuService iMenuService;

    @ApiOperation("首页列表初始化")
    @PostMapping("/index")
    public IResult initHome() {
        Map map = Maps.newHashMap();

        // 轮播
        LambdaQueryWrapper<YxStoreProduct> bannerQueryWrapper = Wrappers.lambdaQuery();
        bannerQueryWrapper.orderByAsc(YxStoreProduct::getSales, YxStoreProduct::getSort).last(" limit 0,5");
        List<YxStoreProduct> banners = iYxStoreProductService.list(bannerQueryWrapper);
        map.put("banners", banners);

        // 好货
        LambdaQueryWrapper<YxStoreProduct> goodQueryWrapper = Wrappers.lambdaQuery();
        goodQueryWrapper.eq(YxStoreProduct::getIsGood, ComYnEnum.YES).orderByAsc(YxStoreProduct::getSort);
        List<YxStoreProduct> hotGoods = iYxStoreProductService.list(goodQueryWrapper);
        map.put("hotGoods", hotGoods);

        // 热销
        LambdaQueryWrapper<YxStoreProduct> hotQueryWrapper = Wrappers.lambdaQuery();
        hotQueryWrapper.eq(YxStoreProduct::getIsHot, ComYnEnum.YES).orderByAsc(YxStoreProduct::getSort);
        List<YxStoreProduct> hotList = iYxStoreProductService.list(hotQueryWrapper);
        map.put("hotList", hotList);

        // 精品
        LambdaQueryWrapper<YxStoreProduct> bestQueryWrapper = Wrappers.lambdaQuery();
        bestQueryWrapper.eq(YxStoreProduct::getIsBest, ComYnEnum.YES).orderByAsc(YxStoreProduct::getSort);
        List<YxStoreProduct> bestList = iYxStoreProductService.list(bestQueryWrapper);
        map.put("bestList", bestList);

        // 首发新货
        LambdaQueryWrapper<YxStoreProduct> firstQueryWrapper = Wrappers.lambdaQuery();
        firstQueryWrapper.eq(YxStoreProduct::getIsNew, ComYnEnum.YES).orderByAsc(YxStoreProduct::getSort);
        List<YxStoreProduct> firstList = iYxStoreProductService.list(firstQueryWrapper);
        map.put("firstList", firstList);

        // 促销
        LambdaQueryWrapper<YxStoreProduct> promoteQueryWrapper = Wrappers.lambdaQuery();
        promoteQueryWrapper.eq(YxStoreProduct::getIsBenefit, ComYnEnum.YES).orderByAsc(YxStoreProduct::getSort);
        List<YxStoreProduct> promoteList = iYxStoreProductService.list(promoteQueryWrapper);
        map.put("promoteList", promoteList);


//        // 菜单
//        LambdaQueryWrapper<Menu> menuQueryWrapper = Wrappers.lambdaQuery();
//        menuQueryWrapper.eq(Menu::getType, MenuType.USER.toString()).orderByAsc(Menu::getSort);
//        List<Menu> menus = iMenuService.list(menuQueryWrapper);
//        map.put("menus", menus.stream().map(menu -> new NavMenu(menu.getId(),menu.getName(),menu.getIcon(),menu.getComponentName(),menu.getPath())).collect(Collectors.toList()));

        map.put("category", iYxStoreCategoryService.getTree(new YxStoreCategory()));

        return IResult.ok(map);
    }

    @ApiOperation("获取查询历史")
    @PostMapping("/history")
    public IResult getMoreProduct() {
        Map map = Maps.newHashMap();
        LambdaQueryWrapper<YxStoreProduct> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(YxStoreProduct::getIsGood, ComYnEnum.YES).orderByAsc(YxStoreProduct::getSort).last(" limit 0,10");
        List<YxStoreProduct> list = iYxStoreProductService.list(queryWrapper);

        map.put("list", list);
        map.put("total", list.size());
        return IResult.ok(map);
    }


    @ApiOperation("翻页获取产品信息")
    @PostMapping(value = "/good/more/{pageNo}/{pageSize}")
    public IResult more(@RequestBody(required = false) SearchParamDto paramDto, @PathVariable long pageNo, @PathVariable long pageSize) {
        IPage<YxStoreProduct> page = new Page<>();
        page.setCurrent(pageNo).setSize(pageSize);
        LambdaQueryWrapper<YxStoreProduct> queryWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(paramDto.getKw())) {
            queryWrapper.like(YxStoreProduct::getKeyword, paramDto.getKw());
        }
        iYxStoreProductService.page(page, queryWrapper);
        return PageResult.builder(page);
    }

    @ApiOperation("通过关键字获取匹配的关键信息")
    @PostMapping(value = "/good/search")
    public IResult searchProduct(@RequestBody SearchParamDto paramDto) {
        LambdaQueryWrapper<YxStoreProduct> queryWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(paramDto.getKw())) {
            queryWrapper.like(YxStoreProduct::getKeyword, paramDto.getKw());
        }
        List<YxStoreProduct> list = iYxStoreProductService.list(queryWrapper.last("limit 0,10"));
        Map map = Maps.newHashMap();
        map.put("list", list);
        map.put("total", list.size());
        return IResult.ok(map);
    }

    @ApiOperation("通过关键字获取匹配的关键信息")
    @PostMapping(value = "/good/keys")
    public IResult searchKey(@RequestBody SearchParamDto paramDto) {
        Map map = Maps.newHashMap();
        LambdaQueryWrapper<YxStoreProduct> queryWrapper = Wrappers.lambdaQuery();
        if (StringUtils.isNotBlank(paramDto.getKw())) {
            queryWrapper.like(YxStoreProduct::getKeyword, paramDto.getKw());
        }
        List list = iYxStoreProductService.list(queryWrapper.last("limit 0,10")).stream().map(YxStoreProduct::getKeyword).collect(Collectors.toList());
        map.put("list", list);
        map.put("total", list.size());
        return IResult.ok(map);
    }











}
