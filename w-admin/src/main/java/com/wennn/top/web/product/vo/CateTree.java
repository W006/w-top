package com.wennn.top.web.product.vo;

import com.wennn.top.util.TreeBuilder;
import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author: wennn
 * @date: 6/2/20
 */
@Data
public class CateTree<T> extends TreeBuilder.BTree<T> {

    private String pic;

    private String comments;
    private String image;
    private String mallCategoryId;
    private String mallCategoryName;
    private List bxMallSubDto;
}
