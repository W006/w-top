package com.wennn.top.web.order.vo;

import com.wennn.top.util.OrderUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * @description: 用于生成临时订单
 * @author: wennn
 * @date: 7/9/20
 */
public class ConfirmOrderVo extends HashMap<String, Object> implements Serializable {

    public ConfirmOrderVo() {
    }

    public ConfirmOrderVo addParam(String key, Object val) {
        this.put(key, val);
        return this;
    }

    // 地址信息 默认地址
    public ConfirmOrderVo setAddress(Object val) {
        return addParam("address", val);
    }

    // 购物车信息
    public ConfirmOrderVo setCartInfos(List val) {
        return addParam("cartInfos", val);
    }

    // 门店信息
    public ConfirmOrderVo setSysStoreInfo(Object val) {
        return addParam("sysStoreInfo", val);
    }

    // 优惠券
    public ConfirmOrderVo setCoupons(List val) {
        return addParam("coupons", val);
    }

    // 用户信息
    public ConfirmOrderVo setUserInfo(Object val) {
        return addParam("userInfo", val);
    }

    // 临时key
    public ConfirmOrderVo setOrderKey(String val) {
        return addParam("orderKey", val);
    }

    public String getOrderKey() {
        return get("orderKey").toString();
    }

    public static ConfirmOrderVo builder() {
        return new ConfirmOrderVo().setOrderKey(OrderUtils.getRandomKey());
    }

}
