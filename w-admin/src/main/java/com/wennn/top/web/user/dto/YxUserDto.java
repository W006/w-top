package com.wennn.top.web.user.dto;

import com.wennn.top.core.mybatisplus.anno.Query;
import lombok.Data;

import java.util.Date;

/**
 * @description:
 * @author: wennn
 * @date: 6/14/20
 */

@Data
public class YxUserDto {

    private Integer id;
    @Query
    private String nickname;
    private String email;
    private String gener;
    @Query
    private String tempAvatar;
    private String avatar;

    private Integer birthday;

    @Query(type = Query.Type.BETWEEN)
    private String[] address;
    @Query(type = Query.Type.BETWEEN)
    private Integer[] ages;
    @Query(type = Query.Type.BETWEEN)
    private Date[] da;


}
