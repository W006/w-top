package com.wennn.top.web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
@ComponentScan(basePackages = {"com.wennn.top"})
public class WAdminApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(WAdminApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("==========================服务启动成功=====================================");
    }

}
