package com.wennn.top.web.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description:
 * @author: wennn
 * @date: 7/18/20
 */

@Data
public class PriceGroupVo {

    private BigDecimal costPrice;
    private BigDecimal storeFreePostage;
    private BigDecimal storePostage; // 邮费
    private BigDecimal totalPrice; // 总价  sum(商品*数量)
    private BigDecimal vipPrice; // vip 价格


}
