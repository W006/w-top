package com.wennn.top.web.home.dto;

import lombok.Data;

@Data
public class SearchParamDto {

    private String keyword;
    private String kw;
}
