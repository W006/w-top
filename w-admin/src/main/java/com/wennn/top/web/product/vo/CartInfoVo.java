package com.wennn.top.web.product.vo;

import com.wennn.top.shop.biz.order.entity.YxStoreCart;
import com.wennn.top.shop.biz.product.entity.YxStoreProduct;
import lombok.Data;

/**
 * @description:
 * @author: wennn
 * @date: 7/8/20
 */
@Data
public class CartInfoVo extends YxStoreCart {

    private YxStoreProduct product;




}
