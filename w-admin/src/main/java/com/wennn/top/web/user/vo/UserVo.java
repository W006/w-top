package com.wennn.top.web.user.vo;

import com.wennn.top.shop.biz.user.entity.YxUser;
import lombok.Data;
import lombok.ToString;

/**
 * @description:
 * @author: wennn
 * @date: 7/7/20
 */

@Data
@ToString
public class UserVo extends YxUser {

}
