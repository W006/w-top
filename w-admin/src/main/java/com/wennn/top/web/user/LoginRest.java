package com.wennn.top.web.user;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.util.CoreConst;
import com.wennn.top.oss.Constant;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.user.entity.YxUser;
import com.wennn.top.shop.biz.user.service.IYxUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @description:
 * @author: wennn
 * @date: 10/9/20
 */
//@RestController
//@RequestMapping("/login")
public class LoginRest {

    @Autowired
    IYxUserService iYxUserService;

    @ApiOperation("获取当前用户信息")
    @PostMapping(value = "/keep")
    public IResult checkLogin(HttpServletRequest request) {


        return IResult.ok(ContextUtil.getUser());
    }

}
