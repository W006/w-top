package com.wennn.top.shop.biz.user.service;

import com.wennn.top.shop.biz.user.entity.YxUserGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户分组表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserGroupService extends IService<YxUserGroup> {

}
