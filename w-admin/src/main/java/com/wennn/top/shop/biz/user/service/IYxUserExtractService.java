package com.wennn.top.shop.biz.user.service;

import com.wennn.top.shop.biz.user.entity.YxUserExtract;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户提现表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserExtractService extends IService<YxUserExtract> {

}
