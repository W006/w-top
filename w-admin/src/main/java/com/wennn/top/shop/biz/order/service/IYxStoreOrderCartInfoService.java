package com.wennn.top.shop.biz.order.service;

import com.wennn.top.shop.biz.order.entity.YxStoreOrderCartInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单购物详情表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
public interface IYxStoreOrderCartInfoService extends IService<YxStoreOrderCartInfo> {

}
