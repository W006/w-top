package com.wennn.top.shop.biz.menu.service.impl;

import com.wennn.top.shop.biz.menu.entity.Menu;
import com.wennn.top.shop.biz.menu.mapper.MenuMapper;
import com.wennn.top.shop.biz.menu.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-05-15
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

}
