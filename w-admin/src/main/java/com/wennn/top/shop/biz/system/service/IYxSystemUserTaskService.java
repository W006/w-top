package com.wennn.top.shop.biz.system.service;

import com.wennn.top.shop.biz.system.entity.YxSystemUserTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 等级任务设置 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface IYxSystemUserTaskService extends IService<YxSystemUserTask> {

}
