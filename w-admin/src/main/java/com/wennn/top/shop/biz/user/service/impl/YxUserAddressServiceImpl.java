package com.wennn.top.shop.biz.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wennn.top.core.mybatis.vo.BEntity;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.user.entity.YxUserAddress;
import com.wennn.top.shop.biz.user.mapper.YxUserAddressMapper;
import com.wennn.top.shop.biz.user.service.IYxUserAddressService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户地址表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserAddressServiceImpl extends ServiceImpl<YxUserAddressMapper, YxUserAddress> implements IYxUserAddressService {

    @Override
    public YxUserAddress getDefaultAddr() {
        List<YxUserAddress> list = list(Wrappers.<YxUserAddress>lambdaQuery().eq(YxUserAddress::getUid, ContextUtil.getUid())
                .eq(YxUserAddress::getIsDefault,true).eq(YxUserAddress::getIsDel, false).orderByAsc(BEntity::getId));
        return list.isEmpty()?null:list.get(0);
    }
}
