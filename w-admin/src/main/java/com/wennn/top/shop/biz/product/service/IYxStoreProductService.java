package com.wennn.top.shop.biz.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wennn.top.shop.biz.product.entity.YxStoreProduct;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxStoreProductService extends IService<YxStoreProduct> {

    Map<Integer,YxStoreProduct> getProduct(List productIds);

}
