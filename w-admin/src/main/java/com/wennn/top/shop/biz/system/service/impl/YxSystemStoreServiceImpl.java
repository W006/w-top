package com.wennn.top.shop.biz.system.service.impl;

import com.wennn.top.shop.biz.system.entity.YxSystemStore;
import com.wennn.top.shop.biz.system.mapper.YxSystemStoreMapper;
import com.wennn.top.shop.biz.system.service.IYxSystemStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店自提 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Service
public class YxSystemStoreServiceImpl extends ServiceImpl<YxSystemStoreMapper, YxSystemStore> implements IYxSystemStoreService {

}
