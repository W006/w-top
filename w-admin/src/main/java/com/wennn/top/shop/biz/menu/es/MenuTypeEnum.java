package com.wennn.top.shop.biz.menu.es;

public enum MenuTypeEnum {
    ADMIN(1),TENANT(2),SHOPPER(3);
    Integer type;

    MenuTypeEnum(Integer i){
        this.type = i;
    }
}
