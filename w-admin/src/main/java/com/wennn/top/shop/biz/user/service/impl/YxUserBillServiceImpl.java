package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserBill;
import com.wennn.top.shop.biz.user.mapper.YxUserBillMapper;
import com.wennn.top.shop.biz.user.service.IYxUserBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账单表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserBillServiceImpl extends ServiceImpl<YxUserBillMapper, YxUserBill> implements IYxUserBillService {

}
