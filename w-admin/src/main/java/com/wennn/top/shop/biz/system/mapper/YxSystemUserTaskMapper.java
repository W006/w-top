package com.wennn.top.shop.biz.system.mapper;

import com.wennn.top.shop.biz.system.entity.YxSystemUserTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 等级任务设置 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface YxSystemUserTaskMapper extends BaseMapper<YxSystemUserTask> {

}
