package com.wennn.top.shop.biz.order.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wennn.top.core.mybatis.vo.BEntity;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.order.entity.YxStoreCart;
import com.wennn.top.shop.biz.order.mapper.YxStoreCartMapper;
import com.wennn.top.shop.biz.order.service.IYxStoreCartService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 购物车表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-05-26
 */
@Service
public class YxStoreCartServiceImpl extends ServiceImpl<YxStoreCartMapper, YxStoreCart> implements IYxStoreCartService {

    /**
     * 获取有效的 购物车信息
     * @param cartIds
     * @return
     */
    @Override
    public List<YxStoreCart> getCarts(String[] cartIds) {
        return list(Wrappers.<YxStoreCart>lambdaQuery().in(BEntity::getId,cartIds).eq(YxStoreCart::getUid, ContextUtil.getUid())
                .eq(YxStoreCart::getIsDel,false).eq(YxStoreCart::getIsPay,false).orderByAsc(YxStoreCart::getAddTime));
    }
}
