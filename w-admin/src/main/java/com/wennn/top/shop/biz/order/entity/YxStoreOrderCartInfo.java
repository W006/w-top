package com.wennn.top.shop.biz.order.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wennn.top.core.mybatis.vo.BEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单购物详情表
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_store_order_cart_info")
@ApiModel(value="YxStoreOrderCartInfo对象", description="订单购物详情表")
public class YxStoreOrderCartInfo extends BEntity<Long> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单id")
    private Integer oid;

    @ApiModelProperty(value = "购物车id")
    private Integer cartId;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @ApiModelProperty(value = "购买东西的详细信息")
    private String cartInfo;

    @ApiModelProperty(value = "唯一id")
    private String unique;


}
