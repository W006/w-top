package com.wennn.top.shop.biz.product.mapper;

import com.wennn.top.shop.biz.product.entity.YxStoreProductCate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品分类辅助表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface YxStoreProductCateMapper extends BaseMapper<YxStoreProductCate> {

}
