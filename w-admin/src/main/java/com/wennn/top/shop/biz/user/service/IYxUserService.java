package com.wennn.top.shop.biz.user.service;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.shop.biz.user.entity.YxUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wennn.top.web.user.dto.YxUserDto;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserService extends IService<YxUser> {

    /**
     * 用户注册
     * @param yxUser
     * @return
     */
    IResult register(YxUser yxUser);

    /**
     * 忘记密码通过预留邮箱找回修改密码
     * @param username
     * @return
     */
    IResult forgetPwd(String username);

    /**
     * 修改用户信息
     * @param inYxUser
     * @param avatarFile
     * @return
     */
    IResult modifyLoginUser(YxUserDto inYxUser,MultipartFile avatarFile);
}
