package com.wennn.top.shop.biz.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wennn.top.shop.biz.product.entity.YxStoreCategory;
import com.wennn.top.shop.biz.product.mapper.YxStoreCategoryMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreCategoryService;
import com.wennn.top.util.TreeBuilder;
import com.wennn.top.web.product.vo.CateTree;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-06-02
 */
@Service
public class YxStoreCategoryServiceImpl extends ServiceImpl<YxStoreCategoryMapper, YxStoreCategory> implements IYxStoreCategoryService {

    @Override
    public List getTree(YxStoreCategory yxStoreCategory) {
        LambdaQueryWrapper<YxStoreCategory> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.orderByAsc(YxStoreCategory::getSort);
        if(!Objects.isNull(yxStoreCategory.getPid())){
            queryWrapper.eq(YxStoreCategory::getPid,yxStoreCategory.getPid());
        }
        List<YxStoreCategory> categories = list(queryWrapper);
        List cates = new ArrayList();
        categories.stream().forEach(i -> {
            CateTree<Long> cateTree = new CateTree();
            cateTree.setId(i.getId());
            cateTree.setPId(i.getPid());
            cateTree.setName(i.getCateName());
            cateTree.setPic(i.getPic());

            cateTree.setImage(i.getPic());
            cateTree.setMallCategoryId(i.getId()+"");
            cateTree.setMallCategoryName(i.getCateName());

            cates.add(cateTree);
        });

        
        return  Objects.isNull(yxStoreCategory.getPid())?TreeBuilder.listToTree(cates):cates;
    }
}
