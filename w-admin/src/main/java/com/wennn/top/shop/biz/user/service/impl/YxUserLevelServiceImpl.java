package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserLevel;
import com.wennn.top.shop.biz.user.mapper.YxUserLevelMapper;
import com.wennn.top.shop.biz.user.service.IYxUserLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户等级记录表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserLevelServiceImpl extends ServiceImpl<YxUserLevelMapper, YxUserLevel> implements IYxUserLevelService {

}
