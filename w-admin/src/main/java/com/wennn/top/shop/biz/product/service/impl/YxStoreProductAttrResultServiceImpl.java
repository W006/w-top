package com.wennn.top.shop.biz.product.service.impl;

import com.wennn.top.shop.biz.product.entity.YxStoreProductAttrResult;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductAttrResultMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductAttrResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品属性详情表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductAttrResultServiceImpl extends ServiceImpl<YxStoreProductAttrResultMapper, YxStoreProductAttrResult> implements IYxStoreProductAttrResultService {

}
