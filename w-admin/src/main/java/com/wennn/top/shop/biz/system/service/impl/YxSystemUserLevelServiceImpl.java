package com.wennn.top.shop.biz.system.service.impl;

import com.wennn.top.shop.biz.system.entity.YxSystemUserLevel;
import com.wennn.top.shop.biz.system.mapper.YxSystemUserLevelMapper;
import com.wennn.top.shop.biz.system.service.IYxSystemUserLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设置用户等级表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Service
public class YxSystemUserLevelServiceImpl extends ServiceImpl<YxSystemUserLevelMapper, YxSystemUserLevel> implements IYxSystemUserLevelService {

}
