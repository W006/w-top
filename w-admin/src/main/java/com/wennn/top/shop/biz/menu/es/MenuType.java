package com.wennn.top.shop.biz.menu.es;

public enum MenuType {
    ADMIN("1"),CUS("2"),USER("3");
    String type;

    MenuType(String type){
        this.type = type;
    }

    @Override
    public String toString() {
        return this.type;
    }
}
