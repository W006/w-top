package com.wennn.top.shop.biz.system.mapper;

import com.wennn.top.shop.biz.system.entity.YxSystemConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 配置表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface YxSystemConfigMapper extends BaseMapper<YxSystemConfig> {

}
