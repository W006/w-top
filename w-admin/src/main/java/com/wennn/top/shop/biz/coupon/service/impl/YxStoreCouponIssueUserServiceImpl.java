package com.wennn.top.shop.biz.coupon.service.impl;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponIssueUser;
import com.wennn.top.shop.biz.coupon.mapper.YxStoreCouponIssueUserMapper;
import com.wennn.top.shop.biz.coupon.service.IYxStoreCouponIssueUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券前台用户领取记录表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
@Service
public class YxStoreCouponIssueUserServiceImpl extends ServiceImpl<YxStoreCouponIssueUserMapper, YxStoreCouponIssueUser> implements IYxStoreCouponIssueUserService {

}
