package com.wennn.top.shop.biz.product.service;

import com.wennn.top.shop.biz.product.entity.YxStoreProductRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品点赞和收藏表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxStoreProductRelationService extends IService<YxStoreProductRelation> {

}
