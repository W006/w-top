package com.wennn.top.shop.biz.coupon.service;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponIssueUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券前台用户领取记录表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface IYxStoreCouponIssueUserService extends IService<YxStoreCouponIssueUser> {

}
