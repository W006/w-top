package com.wennn.top.shop.biz.menu.service;

import com.wennn.top.shop.biz.menu.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wennn
 * @since 2020-05-15
 */
public interface IMenuService extends IService<Menu> {

}
