package com.wennn.top.shop.biz.order.mapper;

import com.wennn.top.shop.biz.order.entity.YxStoreOrderStatus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单操作记录表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
public interface YxStoreOrderStatusMapper extends BaseMapper<YxStoreOrderStatus> {

}
