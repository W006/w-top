package com.wennn.top.shop.biz.user.service;

import com.wennn.top.shop.biz.user.entity.YxUserEnter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商户申请表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserEnterService extends IService<YxUserEnter> {

}
