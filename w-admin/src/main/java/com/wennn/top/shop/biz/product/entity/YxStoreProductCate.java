package com.wennn.top.shop.biz.product.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wennn.top.core.mybatis.vo.BEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 产品分类辅助表
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_store_product_cate")
@ApiModel(value="YxStoreProductCate对象", description="产品分类辅助表")
public class YxStoreProductCate extends BEntity<Long> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品id")
    private Integer productId;

    @ApiModelProperty(value = "分类id")
    private Integer cateId;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;


}
