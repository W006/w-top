package com.wennn.top.shop.biz.system.mapper;

import com.wennn.top.shop.biz.system.entity.YxSystemUserLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设置用户等级表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface YxSystemUserLevelMapper extends BaseMapper<YxSystemUserLevel> {

}
