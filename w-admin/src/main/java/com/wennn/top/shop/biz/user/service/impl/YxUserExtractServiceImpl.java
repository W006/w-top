package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserExtract;
import com.wennn.top.shop.biz.user.mapper.YxUserExtractMapper;
import com.wennn.top.shop.biz.user.service.IYxUserExtractService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户提现表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserExtractServiceImpl extends ServiceImpl<YxUserExtractMapper, YxUserExtract> implements IYxUserExtractService {

}
