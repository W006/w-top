package com.wennn.top.shop.biz.product.mapper;

import com.wennn.top.shop.biz.product.entity.YxStoreProductAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性值表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface YxStoreProductAttrValueMapper extends BaseMapper<YxStoreProductAttrValue> {

}
