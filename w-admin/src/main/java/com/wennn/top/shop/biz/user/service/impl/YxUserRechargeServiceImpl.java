package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserRecharge;
import com.wennn.top.shop.biz.user.mapper.YxUserRechargeMapper;
import com.wennn.top.shop.biz.user.service.IYxUserRechargeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户充值表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserRechargeServiceImpl extends ServiceImpl<YxUserRechargeMapper, YxUserRecharge> implements IYxUserRechargeService {

}
