package com.wennn.top.shop.biz.order.service;

import com.wennn.top.shop.biz.order.entity.YxStoreOrderStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单操作记录表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
public interface IYxStoreOrderStatusService extends IService<YxStoreOrderStatus> {

}
