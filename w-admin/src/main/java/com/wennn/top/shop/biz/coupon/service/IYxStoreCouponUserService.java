package com.wennn.top.shop.biz.coupon.service;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券发放记录表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface IYxStoreCouponUserService extends IService<YxStoreCouponUser> {

}
