package com.wennn.top.shop.biz.system.service;

import com.wennn.top.shop.biz.system.entity.YxSystemConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 配置表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface IYxSystemConfigService extends IService<YxSystemConfig> {

}
