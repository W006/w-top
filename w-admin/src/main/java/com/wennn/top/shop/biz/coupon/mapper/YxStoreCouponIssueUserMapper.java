package com.wennn.top.shop.biz.coupon.mapper;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponIssueUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券前台用户领取记录表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface YxStoreCouponIssueUserMapper extends BaseMapper<YxStoreCouponIssueUser> {

}
