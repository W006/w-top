package com.wennn.top.shop.biz.user.mapper;

import com.wennn.top.shop.biz.user.entity.YxUserLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户等级记录表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface YxUserLevelMapper extends BaseMapper<YxUserLevel> {

}
