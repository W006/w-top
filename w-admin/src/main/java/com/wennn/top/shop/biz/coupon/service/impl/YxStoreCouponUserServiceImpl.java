package com.wennn.top.shop.biz.coupon.service.impl;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponUser;
import com.wennn.top.shop.biz.coupon.mapper.YxStoreCouponUserMapper;
import com.wennn.top.shop.biz.coupon.service.IYxStoreCouponUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券发放记录表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
@Service
public class YxStoreCouponUserServiceImpl extends ServiceImpl<YxStoreCouponUserMapper, YxStoreCouponUser> implements IYxStoreCouponUserService {

}
