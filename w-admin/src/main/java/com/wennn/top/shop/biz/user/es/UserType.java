package com.wennn.top.shop.biz.user.es;

import lombok.Getter;

//用户类型[0:管理员，1:商户，2:用户]
public enum UserType {
    ADMIN("0"),POS("1"),USER("2");

    @Getter
    String code;

    UserType(String code){
        this.code = code;
    }

}
