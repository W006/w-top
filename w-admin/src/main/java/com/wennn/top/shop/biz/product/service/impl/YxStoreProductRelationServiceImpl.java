package com.wennn.top.shop.biz.product.service.impl;

import com.wennn.top.shop.biz.product.entity.YxStoreProductRelation;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductRelationMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品点赞和收藏表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductRelationServiceImpl extends ServiceImpl<YxStoreProductRelationMapper, YxStoreProductRelation> implements IYxStoreProductRelationService {

}
