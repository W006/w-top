package com.wennn.top.shop.biz.user.service;

import com.wennn.top.shop.biz.user.entity.YxUserBill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户账单表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserBillService extends IService<YxUserBill> {

}
