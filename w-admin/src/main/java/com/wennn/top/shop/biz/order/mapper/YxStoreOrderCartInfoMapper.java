package com.wennn.top.shop.biz.order.mapper;

import com.wennn.top.shop.biz.order.entity.YxStoreOrderCartInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单购物详情表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
public interface YxStoreOrderCartInfoMapper extends BaseMapper<YxStoreOrderCartInfo> {

}
