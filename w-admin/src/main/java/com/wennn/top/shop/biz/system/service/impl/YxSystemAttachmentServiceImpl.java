package com.wennn.top.shop.biz.system.service.impl;

import com.wennn.top.shop.biz.system.entity.YxSystemAttachment;
import com.wennn.top.shop.biz.system.mapper.YxSystemAttachmentMapper;
import com.wennn.top.shop.biz.system.service.IYxSystemAttachmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 附件管理表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Service
public class YxSystemAttachmentServiceImpl extends ServiceImpl<YxSystemAttachmentMapper, YxSystemAttachment> implements IYxSystemAttachmentService {

}
