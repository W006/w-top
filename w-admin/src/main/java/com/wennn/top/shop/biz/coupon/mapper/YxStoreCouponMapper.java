package com.wennn.top.shop.biz.coupon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wennn.top.shop.biz.coupon.dto.StoreCouponDto;
import com.wennn.top.shop.biz.coupon.entity.YxStoreCoupon;

import java.util.List;

/**
 * <p>
 * 优惠券表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface YxStoreCouponMapper extends BaseMapper<YxStoreCoupon> {


    /**
     * 获取系统发放的优惠券
     * @param uid
     * @return
     */
    public List<StoreCouponDto> getSysUserCoupon(Integer uid);

    /**
     * 获取用户自己领取的优惠券
     * @param uid
     * @return
     */
    public List<StoreCouponDto> getUserCoupon(Integer uid);

}
