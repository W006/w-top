package com.wennn.top.shop.biz.user.mapper;

import com.wennn.top.shop.biz.user.entity.YxUserTaskFinish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户任务完成记录表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface YxUserTaskFinishMapper extends BaseMapper<YxUserTaskFinish> {

}
