package com.wennn.top.shop.biz.product.mapper;

import com.wennn.top.shop.biz.product.entity.YxStoreProductReply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface YxStoreProductReplyMapper extends BaseMapper<YxStoreProductReply> {

}
