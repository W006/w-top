package com.wennn.top.shop.biz.system.service;

import com.wennn.top.shop.biz.system.entity.YxSystemStore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 门店自提 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface IYxSystemStoreService extends IService<YxSystemStore> {

}
