package com.wennn.top.shop.biz.order.mapper;

import com.wennn.top.shop.biz.order.entity.YxStoreOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
public interface YxStoreOrderMapper extends BaseMapper<YxStoreOrder> {

    public List<Map<String,Integer>> countStatus(@Param("status") String status,@Param("uid")String uid);
}
