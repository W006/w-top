package com.wennn.top.shop.biz.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wennn.top.core.mybatis.vo.BEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 用户充值表
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_user_recharge")
@ApiModel(value="YxUserRecharge对象", description="用户充值表")
public class YxUserRecharge extends BEntity<Long> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "充值用户UID")
    private Integer uid;

    private String nickname;

    @ApiModelProperty(value = "订单号")
    private String orderId;

    @ApiModelProperty(value = "充值金额")
    private BigDecimal price;

    @ApiModelProperty(value = "充值类型")
    private String rechargeType;

    @ApiModelProperty(value = "是否充值")
    private Boolean paid;

    @ApiModelProperty(value = "充值支付时间")
    private Integer payTime;

    @ApiModelProperty(value = "充值时间")
    private Integer addTime;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundPrice;


}
