package com.wennn.top.shop.biz.product.service;

import com.wennn.top.shop.biz.product.entity.YxStoreProductAttrResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品属性详情表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxStoreProductAttrResultService extends IService<YxStoreProductAttrResult> {

}
