package com.wennn.top.shop.biz.product.entity;

import com.wennn.top.core.mybatis.vo.BEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分类表
 * </p>
 *
 * @author wennn
 * @since 2020-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="YxStoreCategory对象", description="商品分类表")
public class YxStoreCategory extends BEntity<Long> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父id")
    private Long pid;

    @ApiModelProperty(value = "分类名称")
    private String cateName;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "图标")
    private String pic;

    @ApiModelProperty(value = "是否推荐")
    private Boolean isShow;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;


}
