package com.wennn.top.shop.biz.order.service.impl;

import com.wennn.top.shop.biz.order.entity.YxStoreOrderStatus;
import com.wennn.top.shop.biz.order.mapper.YxStoreOrderStatusMapper;
import com.wennn.top.shop.biz.order.service.IYxStoreOrderStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单操作记录表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
@Service
public class YxStoreOrderStatusServiceImpl extends ServiceImpl<YxStoreOrderStatusMapper, YxStoreOrderStatus> implements IYxStoreOrderStatusService {

}
