package com.wennn.top.shop.biz.coupon.service.impl;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponIssue;
import com.wennn.top.shop.biz.coupon.mapper.YxStoreCouponIssueMapper;
import com.wennn.top.shop.biz.coupon.service.IYxStoreCouponIssueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券前台领取表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
@Service
public class YxStoreCouponIssueServiceImpl extends ServiceImpl<YxStoreCouponIssueMapper, YxStoreCouponIssue> implements IYxStoreCouponIssueService {

}
