package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserSign;
import com.wennn.top.shop.biz.user.mapper.YxUserSignMapper;
import com.wennn.top.shop.biz.user.service.IYxUserSignService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到记录表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserSignServiceImpl extends ServiceImpl<YxUserSignMapper, YxUserSign> implements IYxUserSignService {

}
