package com.wennn.top.shop.biz.system.service.impl;

import com.wennn.top.shop.biz.system.entity.YxSystemUserTask;
import com.wennn.top.shop.biz.system.mapper.YxSystemUserTaskMapper;
import com.wennn.top.shop.biz.system.service.IYxSystemUserTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 等级任务设置 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Service
public class YxSystemUserTaskServiceImpl extends ServiceImpl<YxSystemUserTaskMapper, YxSystemUserTask> implements IYxSystemUserTaskService {

}
