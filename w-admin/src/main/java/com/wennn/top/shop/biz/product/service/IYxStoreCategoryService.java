package com.wennn.top.shop.biz.product.service;

import com.wennn.top.shop.biz.product.entity.YxStoreCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-06-02
 */
public interface IYxStoreCategoryService extends IService<YxStoreCategory> {


    public List getTree(YxStoreCategory yxStoreCategory);

}
