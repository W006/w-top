package com.wennn.top.shop.biz.system.service;

import com.wennn.top.shop.biz.system.entity.YxSystemAttachment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 附件管理表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface IYxSystemAttachmentService extends IService<YxSystemAttachment> {

}
