package com.wennn.top.shop.biz.product.mapper;

import com.wennn.top.shop.biz.product.entity.YxStoreCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-06-02
 */
public interface YxStoreCategoryMapper extends BaseMapper<YxStoreCategory> {

}
