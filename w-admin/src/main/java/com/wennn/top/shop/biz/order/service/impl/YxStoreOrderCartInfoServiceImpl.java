package com.wennn.top.shop.biz.order.service.impl;

import com.wennn.top.shop.biz.order.entity.YxStoreOrderCartInfo;
import com.wennn.top.shop.biz.order.mapper.YxStoreOrderCartInfoMapper;
import com.wennn.top.shop.biz.order.service.IYxStoreOrderCartInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单购物详情表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
@Service
public class YxStoreOrderCartInfoServiceImpl extends ServiceImpl<YxStoreOrderCartInfoMapper, YxStoreOrderCartInfo> implements IYxStoreOrderCartInfoService {

}
