package com.wennn.top.shop.biz.product.es;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum RelationType {

    COLLECT("collect","收藏"),LIKE("like","喜欢");

    @Getter
    private String code;

    @Getter
    private String desc;


}
