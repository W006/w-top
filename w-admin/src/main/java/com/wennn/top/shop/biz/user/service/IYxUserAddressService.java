package com.wennn.top.shop.biz.user.service;

import com.wennn.top.shop.biz.user.entity.YxUserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户地址表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserAddressService extends IService<YxUserAddress> {

    /**
     * 获取当前登陆用户的默认地址
     * @return
     */
    public YxUserAddress getDefaultAddr();

}
