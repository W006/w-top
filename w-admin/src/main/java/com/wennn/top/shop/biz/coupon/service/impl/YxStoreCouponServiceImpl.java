package com.wennn.top.shop.biz.coupon.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.wennn.top.security.util.ContextUtil;
import com.wennn.top.shop.biz.coupon.dto.StoreCouponDto;
import com.wennn.top.shop.biz.coupon.entity.YxStoreCoupon;
import com.wennn.top.shop.biz.coupon.mapper.YxStoreCouponMapper;
import com.wennn.top.shop.biz.coupon.service.IYxStoreCouponService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 优惠券表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
@Service
public class YxStoreCouponServiceImpl extends ServiceImpl<YxStoreCouponMapper, YxStoreCoupon> implements IYxStoreCouponService {

    @Override
    public List<YxStoreCoupon> getUserCoupons() {
        // 1. 系统发放的优惠券
        List<StoreCouponDto> sysUserCoupon = getBaseMapper().getSysUserCoupon(ContextUtil.getUid());

        // 2. 用户自己领取的优惠券
        List<StoreCouponDto> userCoupon = getBaseMapper().getUserCoupon(ContextUtil.getUid());

        List all = Lists.newArrayList();
        all.addAll(sysUserCoupon);
        all.addAll(userCoupon);

        return all;
    }
}
