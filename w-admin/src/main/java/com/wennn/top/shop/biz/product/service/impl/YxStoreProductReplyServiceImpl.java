package com.wennn.top.shop.biz.product.service.impl;

import com.wennn.top.shop.biz.product.entity.YxStoreProductReply;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductReplyMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductReplyServiceImpl extends ServiceImpl<YxStoreProductReplyMapper, YxStoreProductReply> implements IYxStoreProductReplyService {

}
