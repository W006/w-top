package com.wennn.top.shop.biz.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wennn.top.shop.biz.order.entity.YxStoreCart;

import java.util.List;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-05-26
 */
public interface IYxStoreCartService extends IService<YxStoreCart> {

    public List<YxStoreCart> getCarts(String[] cartIds);
}
