package com.wennn.top.shop.biz.user.service;

import com.wennn.top.shop.biz.user.entity.YxUserSign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 签到记录表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxUserSignService extends IService<YxUserSign> {

}
