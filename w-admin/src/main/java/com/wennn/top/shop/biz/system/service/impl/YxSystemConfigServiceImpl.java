package com.wennn.top.shop.biz.system.service.impl;

import com.wennn.top.shop.biz.system.entity.YxSystemConfig;
import com.wennn.top.shop.biz.system.mapper.YxSystemConfigMapper;
import com.wennn.top.shop.biz.system.service.IYxSystemConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配置表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Service
public class YxSystemConfigServiceImpl extends ServiceImpl<YxSystemConfigMapper, YxSystemConfig> implements IYxSystemConfigService {

}
