package com.wennn.top.shop.biz.system.mapper;

import com.wennn.top.shop.biz.system.entity.YxSystemStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 门店自提 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface YxSystemStoreMapper extends BaseMapper<YxSystemStore> {

}
