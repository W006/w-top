package com.wennn.top.shop.biz.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 组合数据详情表
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_system_group_data")
@ApiModel(value="YxSystemGroupData对象", description="组合数据详情表")
public class YxSystemGroupData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "对应的数据名称")
    private String groupName;

    @ApiModelProperty(value = "数据组对应的数据值（json数据）")
    private String value;

    @ApiModelProperty(value = "添加数据时间")
    private Integer addTime;

    @ApiModelProperty(value = "数据排序")
    private Integer sort;

    @ApiModelProperty(value = "状态（1：开启；2：关闭；）")
    private Boolean status;


}
