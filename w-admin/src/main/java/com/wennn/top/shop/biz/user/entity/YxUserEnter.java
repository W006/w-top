package com.wennn.top.shop.biz.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wennn.top.core.mybatis.vo.BEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商户申请表
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_user_enter")
@ApiModel(value="YxUserEnter对象", description="商户申请表")
public class YxUserEnter extends BEntity<Long> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private Integer uid;

    @ApiModelProperty(value = "商户所在省")
    private String province;

    @ApiModelProperty(value = "商户所在市")
    private String city;

    @ApiModelProperty(value = "商户所在区")
    private String district;

    @ApiModelProperty(value = "商户详细地址")
    private String address;

    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    private String linkUser;

    @ApiModelProperty(value = "商户电话")
    private String linkTel;

    @ApiModelProperty(value = "商户证书")
    private String charter;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @ApiModelProperty(value = "审核时间")
    private Integer applyTime;

    @ApiModelProperty(value = "通过时间")
    private Integer successTime;

    @ApiModelProperty(value = "未通过原因")
    private String failMessage;

    @ApiModelProperty(value = "未通过时间")
    private Integer failTime;

    @ApiModelProperty(value = "-1 审核未通过 0未审核 1审核通过")
    private Boolean status;

    @ApiModelProperty(value = "0 = 开启 1= 关闭")
    private Boolean isLock;

    @ApiModelProperty(value = "是否删除")
    private Boolean isDel;


}
