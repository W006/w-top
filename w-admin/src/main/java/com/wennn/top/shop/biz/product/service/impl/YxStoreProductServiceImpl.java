package com.wennn.top.shop.biz.product.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.google.common.collect.Maps;
import com.wennn.top.core.mybatis.vo.BEntity;
import com.wennn.top.shop.biz.product.entity.YxStoreProduct;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductServiceImpl extends ServiceImpl<YxStoreProductMapper, YxStoreProduct> implements IYxStoreProductService {

    /**
     * 获取db 中的有效订单
     * @param productIds
     * @return
     */
    @Override
    public Map<Integer,YxStoreProduct> getProduct(List productIds) {
        Map m = Maps.newHashMap();
        list(Wrappers.<YxStoreProduct>lambdaQuery().eq(YxStoreProduct::getIsDel,false).in(BEntity::getId,productIds))
                .parallelStream().forEach(product->{
                    m.put(product.getId(),product);
        });
        return m;
    }
}
