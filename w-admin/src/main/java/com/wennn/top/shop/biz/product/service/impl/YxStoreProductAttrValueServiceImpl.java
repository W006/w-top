package com.wennn.top.shop.biz.product.service.impl;

import com.wennn.top.shop.biz.product.entity.YxStoreProductAttrValue;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductAttrValueMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductAttrValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品属性值表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductAttrValueServiceImpl extends ServiceImpl<YxStoreProductAttrValueMapper, YxStoreProductAttrValue> implements IYxStoreProductAttrValueService {

}
