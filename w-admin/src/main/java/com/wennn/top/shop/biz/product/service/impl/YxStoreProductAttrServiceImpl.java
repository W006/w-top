package com.wennn.top.shop.biz.product.service.impl;

import com.wennn.top.shop.biz.product.entity.YxStoreProductAttr;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductAttrMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductAttrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品属性表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductAttrServiceImpl extends ServiceImpl<YxStoreProductAttrMapper, YxStoreProductAttr> implements IYxStoreProductAttrService {

}
