package com.wennn.top.shop.biz.system.mapper;

import com.wennn.top.shop.biz.system.entity.YxSystemGroupData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 组合数据详情表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface YxSystemGroupDataMapper extends BaseMapper<YxSystemGroupData> {

}
