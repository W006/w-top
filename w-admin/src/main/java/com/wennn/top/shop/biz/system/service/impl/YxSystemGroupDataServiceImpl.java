package com.wennn.top.shop.biz.system.service.impl;

import com.wennn.top.shop.biz.system.entity.YxSystemGroupData;
import com.wennn.top.shop.biz.system.mapper.YxSystemGroupDataMapper;
import com.wennn.top.shop.biz.system.service.IYxSystemGroupDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组合数据详情表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
@Service
public class YxSystemGroupDataServiceImpl extends ServiceImpl<YxSystemGroupDataMapper, YxSystemGroupData> implements IYxSystemGroupDataService {

}
