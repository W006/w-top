package com.wennn.top.shop.biz.coupon.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 优惠券前台用户领取记录表
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_store_coupon_issue_user")
@ApiModel(value="YxStoreCouponIssueUser对象", description="优惠券前台用户领取记录表")
public class YxStoreCouponIssueUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "领取优惠券用户ID")
    private Integer uid;

    @ApiModelProperty(value = "优惠券前台领取ID")
    private Integer issueCouponId;

    @ApiModelProperty(value = "领取时间")
    private Integer addTime;


}
