package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserEnter;
import com.wennn.top.shop.biz.user.mapper.YxUserEnterMapper;
import com.wennn.top.shop.biz.user.service.IYxUserEnterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商户申请表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserEnterServiceImpl extends ServiceImpl<YxUserEnterMapper, YxUserEnter> implements IYxUserEnterService {

}
