package com.wennn.top.shop.biz.coupon.service;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponIssue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券前台领取表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface IYxStoreCouponIssueService extends IService<YxStoreCouponIssue> {

}
