package com.wennn.top.shop.biz.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wennn.top.common.vo.IResult;
import com.wennn.top.shop.biz.order.entity.YxStoreOrder;
import com.wennn.top.web.order.dto.OrderCreateDto;
import com.wennn.top.web.order.vo.ConfirmOrderVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-05-14
 */
public interface IYxStoreOrderService extends IService<YxStoreOrder> {

    public ConfirmOrderVo confirm(String[] cartIds);

    public IResult createOrder(ConfirmOrderVo vo, OrderCreateDto dto);

    public  List<Map<String, Integer>> statusCount(String status,String uid);
}
