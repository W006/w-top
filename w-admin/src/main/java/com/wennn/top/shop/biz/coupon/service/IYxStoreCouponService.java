package com.wennn.top.shop.biz.coupon.service;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCoupon;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 优惠券表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface IYxStoreCouponService extends IService<YxStoreCoupon> {


    List<YxStoreCoupon> getUserCoupons();
}
