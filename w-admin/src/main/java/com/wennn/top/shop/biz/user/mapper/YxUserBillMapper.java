package com.wennn.top.shop.biz.user.mapper;

import com.wennn.top.shop.biz.user.entity.YxUserBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户账单表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface YxUserBillMapper extends BaseMapper<YxUserBill> {

}
