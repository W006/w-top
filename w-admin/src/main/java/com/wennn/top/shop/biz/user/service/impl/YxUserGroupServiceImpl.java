package com.wennn.top.shop.biz.user.service.impl;

import com.wennn.top.shop.biz.user.entity.YxUserGroup;
import com.wennn.top.shop.biz.user.mapper.YxUserGroupMapper;
import com.wennn.top.shop.biz.user.service.IYxUserGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户分组表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxUserGroupServiceImpl extends ServiceImpl<YxUserGroupMapper, YxUserGroup> implements IYxUserGroupService {

}
