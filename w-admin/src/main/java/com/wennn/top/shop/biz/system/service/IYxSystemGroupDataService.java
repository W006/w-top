package com.wennn.top.shop.biz.system.service;

import com.wennn.top.shop.biz.system.entity.YxSystemGroupData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 组合数据详情表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface IYxSystemGroupDataService extends IService<YxSystemGroupData> {

}
