package com.wennn.top.shop.biz.product.service;

import com.wennn.top.shop.biz.product.entity.YxStoreProductCate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 产品分类辅助表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxStoreProductCateService extends IService<YxStoreProductCate> {

}
