package com.wennn.top.shop.biz.coupon.mapper;

import com.wennn.top.shop.biz.coupon.entity.YxStoreCouponIssue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券前台领取表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
public interface YxStoreCouponIssueMapper extends BaseMapper<YxStoreCouponIssue> {

}
