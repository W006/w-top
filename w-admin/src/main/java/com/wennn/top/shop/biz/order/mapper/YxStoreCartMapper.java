package com.wennn.top.shop.biz.order.mapper;

import com.wennn.top.shop.biz.order.entity.YxStoreCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-05-26
 */
public interface YxStoreCartMapper extends BaseMapper<YxStoreCart> {

}
