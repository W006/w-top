package com.wennn.top.shop.biz.product.service;

import com.wennn.top.shop.biz.product.entity.YxStoreProductReply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
public interface IYxStoreProductReplyService extends IService<YxStoreProductReply> {

}
