package com.wennn.top.shop.biz.coupon.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 优惠券前台领取表
 * </p>
 *
 * @author wennn
 * @since 2020-07-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("yx_store_coupon_issue")
@ApiModel(value="YxStoreCouponIssue对象", description="优惠券前台领取表")
public class YxStoreCouponIssue implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cname;

    @ApiModelProperty(value = "优惠券ID")
    private Integer cid;

    @ApiModelProperty(value = "优惠券领取开启时间")
    private Integer startTime;

    @ApiModelProperty(value = "优惠券领取结束时间")
    private Integer endTime;

    @ApiModelProperty(value = "优惠券领取数量")
    private Integer totalCount;

    @ApiModelProperty(value = "优惠券剩余领取数量")
    private Integer remainCount;

    @ApiModelProperty(value = "是否无限张数")
    private Boolean isPermanent;

    @ApiModelProperty(value = "1 正常 0 未开启 -1 已无效")
    private Boolean status;

    private Boolean isDel;

    @ApiModelProperty(value = "优惠券添加时间")
    private Integer addTime;

    private LocalDateTime endTimeDate;

    private LocalDateTime startTimeDate;


}
