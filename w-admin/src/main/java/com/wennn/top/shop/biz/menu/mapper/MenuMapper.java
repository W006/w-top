package com.wennn.top.shop.biz.menu.mapper;

import com.wennn.top.shop.biz.menu.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-05-15
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
