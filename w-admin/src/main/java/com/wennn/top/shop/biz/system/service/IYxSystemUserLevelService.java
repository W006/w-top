package com.wennn.top.shop.biz.system.service;

import com.wennn.top.shop.biz.system.entity.YxSystemUserLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设置用户等级表 服务类
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface IYxSystemUserLevelService extends IService<YxSystemUserLevel> {

}
