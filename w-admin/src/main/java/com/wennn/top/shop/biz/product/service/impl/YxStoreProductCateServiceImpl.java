package com.wennn.top.shop.biz.product.service.impl;

import com.wennn.top.shop.biz.product.entity.YxStoreProductCate;
import com.wennn.top.shop.biz.product.mapper.YxStoreProductCateMapper;
import com.wennn.top.shop.biz.product.service.IYxStoreProductCateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 产品分类辅助表 服务实现类
 * </p>
 *
 * @author wennn
 * @since 2020-04-18
 */
@Service
public class YxStoreProductCateServiceImpl extends ServiceImpl<YxStoreProductCateMapper, YxStoreProductCate> implements IYxStoreProductCateService {

}
