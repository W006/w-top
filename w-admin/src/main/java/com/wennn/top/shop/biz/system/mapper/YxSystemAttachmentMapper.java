package com.wennn.top.shop.biz.system.mapper;

import com.wennn.top.shop.biz.system.entity.YxSystemAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 附件管理表 Mapper 接口
 * </p>
 *
 * @author wennn
 * @since 2020-07-08
 */
public interface YxSystemAttachmentMapper extends BaseMapper<YxSystemAttachment> {

}
