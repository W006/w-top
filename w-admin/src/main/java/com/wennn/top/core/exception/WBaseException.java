package com.wennn.top.core.exception;

import com.wennn.top.common.vo.IResult;
import lombok.Getter;

/**
 * @description: 统一异常
 * @author: wennn
 * @date: 5/24/20
 */
public class WBaseException extends RuntimeException {

    @Getter
    private IResult result;

    public WBaseException(){}

    public WBaseException(IResult result){
        this.result = result;
    }

    public static WBaseException throwNew(){
        return new WBaseException();
    }

}
