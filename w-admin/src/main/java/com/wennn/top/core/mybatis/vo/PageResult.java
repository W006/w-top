package com.wennn.top.core.mybatis.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wennn.top.common.vo.IResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: wennn
 * @date: 5/24/20
 */
public class PageResult extends IResult<Map> {

    public static PageResult builder(IPage page){
        PageResult pageResult = new PageResult();
        Map map = new HashMap(2,1f  );
        map.put("total",page.getTotal());
        map.put("list",page.getRecords());
        pageResult.setData(map);
        pageResult.setCode(OK);
        return pageResult;
    }

}
