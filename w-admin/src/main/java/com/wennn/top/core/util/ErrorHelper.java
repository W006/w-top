package com.wennn.top.core.util;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.exception.WBusiException;

import java.util.Optional;

/**
 * @description: 错误处理
 * @author: wennn
 * @date: 5/24/20
 */
public class ErrorHelper {
    public static IResult ifPresent(Object one) {
        return IResult.ok(Optional.ofNullable(one).orElseThrow(()->new WBusiException(IResult.fail(CoreConst.NO_FOUND,"未找到结果"))));
    }

    public static IResult update(Boolean flag) {
        return flag?IResult.ok().msg("更新成功"):IResult.fail("更新失败");
    }

    public static IResult save(Boolean flag) {
        return flag?IResult.ok().msg("新增成功"):IResult.fail("新增失败");
    }

    public static IResult delete(Boolean flag) {
        return flag?IResult.ok().msg("删除成功"):IResult.fail("删除失败");
    }

}
