package com.wennn.top.core.util;

/**
 * @description: 核心代码常量
 * @author: wennn
 * @date: 5/24/20
 */
public class CoreConst {

    /**无权限*/
    public static final String NO_AUTH = "401";
    /**成功*/
    public static final String SUCCESS = "200";

    /**未找到*/
    public static final String NO_FOUND = "404";

    /**异常处理*/
    public static final String ERROR = "500";

    /**业务处理失败*/
    public static final String FAIL = "501";
}
