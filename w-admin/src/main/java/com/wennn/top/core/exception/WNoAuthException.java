package com.wennn.top.core.exception;

import com.wennn.top.common.vo.IResult;

/**
 * @description: 无权访问异常
 * @author: wennn
 * @date: 5/24/20
 */
public class WNoAuthException extends WBaseException {
    public WNoAuthException() {
        super();
    }

    public WNoAuthException(IResult result) {
        super(result);
    }
}
