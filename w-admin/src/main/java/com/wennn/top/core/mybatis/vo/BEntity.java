package com.wennn.top.core.mybatis.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class BEntity<K> implements Serializable {
    @TableId(value = "id",type = IdType.AUTO)
    K id;
}
