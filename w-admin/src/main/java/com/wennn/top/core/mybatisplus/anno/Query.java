package com.wennn.top.core.mybatisplus.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 查询构建 用于生产查询条件
 * @author: wennn
 * @date: 7/7/20 4:53 PM
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Query {

    // Dong ZhaoYang 2017/8/7 基本对象的属性名
    String dbName() default "";

    // Dong ZhaoYang 2017/8/7 查询方式
    Type type() default Type.EQ;

    /**
     * 多字段模糊搜索，仅支持String类型字段，多个用逗号隔开, 如@Query(blurry = "email,username")
     */
    String blurry() default "";

    enum Type {
        EQ,
        NE,
        GT,
        GE,
        LT,
        LE,
        BETWEEN,
        NOTBETWEEN,
        LIKE,
        NOTLIKE,
        LIKELEFT,
        LIKERIGHT,
        ISNULL,
        ISNOTNULL,
        IN,
        NOTIN,
        INSQL,
        NOTINSQL,
        GROUPBY,
        ORDERBYASC,
        ORDERBYDESC
    }

}