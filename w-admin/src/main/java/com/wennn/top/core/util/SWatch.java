package com.wennn.top.core.util;

import org.springframework.util.StopWatch;

import java.text.NumberFormat;

/**
 * @description:
 * @author: wennn
 * @date: 7/13/20
 */
public class SWatch extends StopWatch {

    public SWatch() {
    }

    public SWatch(String id) {
        super(id);
    }

    public String prettyResult() {
        StringBuilder sb = new StringBuilder(this.shortSummary());
        sb.append('\n');
        sb.append("---------------------------------------------\n");
        sb.append("seconds         %     Task name\n");
        sb.append("---------------------------------------------\n");
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMinimumIntegerDigits(9);
        nf.setGroupingUsed(false);
        NumberFormat pf = NumberFormat.getPercentInstance();
        pf.setMinimumIntegerDigits(3);
        pf.setGroupingUsed(false);
        StopWatch.TaskInfo[] var4 = this.getTaskInfo();
        int var5 = var4.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            StopWatch.TaskInfo task = var4[var6];
            sb.append(nf.format(task.getTimeSeconds())).append("  ");
            sb.append(pf.format((double)task.getTimeNanos() / (double)this.getTotalTimeNanos())).append("  ");
            sb.append(task.getTaskName()).append("\n");
        }
        return sb.toString();
    }

}
