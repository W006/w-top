package com.wennn.top.core.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: wennn
 * @date: 10/1/20
 */
@Component
public class SpringUtils implements ApplicationContextAware {
        public static ApplicationContext applicationContext;

        @Override
        public void setApplicationContext(ApplicationContext applicationContext)
                throws BeansException {
            this.applicationContext = applicationContext;
        }

        public static Object getBean(String name) {
            return applicationContext.getBean(name);
        }

        public static <T> T getBean(String name, Class<T> requiredType) {
            return applicationContext.getBean(name, requiredType);
        }

        public static boolean containsBean(String name) {
            return applicationContext.containsBean(name);
        }

        public static boolean isSingleton(String name) {
            return applicationContext.isSingleton(name);
        }

        public static Class<? extends Object> getType(String name) {
            return applicationContext.getType(name);
        }

        /**
         * 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型.
         */
        public static <T> T getBean(Class<T> requiredType) {
            return applicationContext.getBean(requiredType);
        }

}
