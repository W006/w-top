package com.wennn.top.core.exception;

import com.wennn.top.common.vo.IResult;
import com.wennn.top.core.util.CoreConst;

/**
 * @description: 业务事物回滚错误
 * @author: wennn
 * @date: 5/24/20
 */
public class WBusiException extends WBaseException {

    public WBusiException() {
        super();
    }

    public WBusiException(String msg) {
        super(IResult.fail(CoreConst.ERROR,msg));
    }

    public WBusiException(IResult result) {
        super(result);
    }
}
