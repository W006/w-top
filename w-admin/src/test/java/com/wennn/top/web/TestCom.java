package com.wennn.top.web;

import java.util.*;

/**
 * @description:
 * @author: wennn
 * @date: 6/2/20
 */
public class TestCom {

    public static void main(String[] args) {
        String input = "56 65 74 100 99 68 86 180 90";
        String[] arr = input.split(" ");
        Arrays.stream(arr).map(i->{
            char[] chars =  i.toCharArray();
            int sum = 0;
            for (char c:chars){
                sum =sum+ Integer.valueOf(c+"");
            }
            Map map = new HashMap();
            map.put("word",i);
            map.put("sum",sum);
            return map;
        }).sorted((o1,o2)->Integer.valueOf(o1.get("sum")+"") - Integer.valueOf(o2.get("sum")+"")).map(i->i.get("word")).forEach(i->System.out.print(i+" "));


    }
}
