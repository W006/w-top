package com.wennn.top.swagger.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "swagger")
@Component
@Data
@ToString
public class SwaggerProp {

    private boolean enable;

    private String title;

    private String groupName;

    private String basePackage;

    private String description;

    private String serverUrl;

    private String contactName;
    private String contactUrl;
    private String contactEmail;

    private String version;


}
