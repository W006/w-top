package com.wennn.top.swagger.controller;

import com.wennn.top.swagger.config.SwaggerProp;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sdemo")
@Api(tags = "测试代码",description = "这个就是测试用的")
public class SDemoController {

    @Autowired
    SwaggerProp swaggerProp;

    @GetMapping("/doDemo")
    @ApiOperation(value = "demo 测试",notes = "注意是测试")
    public SwaggerProp doDemo(){
        System.out.println(swaggerProp);
        return swaggerProp;
    }


}
