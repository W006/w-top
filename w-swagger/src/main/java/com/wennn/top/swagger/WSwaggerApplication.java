package com.wennn.top.swagger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@SpringBootApplication
public class WSwaggerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(WSwaggerApplication.class, args);
    }



    @Override
    public void run(String... args) throws Exception {
        for (String arg:args){
            System.out.println(arg);
        }
        System.out.println("==========================服务启动成功=====================================");
    }
}
