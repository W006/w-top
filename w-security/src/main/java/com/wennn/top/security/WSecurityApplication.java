package com.wennn.top.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(WSecurityApplication.class, args);
    }

}
