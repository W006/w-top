package com.wennn.top.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class WCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(WCommonApplication.class, args);
    }

}
